import pandas as pd
from process_geojson import df_from_geojson, histogram

GEOJSON_NAME = 'obitos_infantis.geojson'

df = df_from_geojson(data=GEOJSON_NAME,
                     neighbourhoods='BAIRRO',
                     values='TAXA_OBITOS_INFANTIS',
                     metadata=['QTDE_OBITOS_INFANTIS','QTDE_NASCIDOS_VIVOS'])

df_hist = histogram(df,'TAXA_OBITOS_INFANTIS')

df_hist.to_csv(f'../processed data/hist_{GEOJSON_NAME.split(".")[0]}.csv', index=False)

df_indicators = pd.read_csv(f'../processed data/table_{GEOJSON_NAME.split(".")[0]}.csv').describe()
 
print(df_indicators)
