from process_geojson import df_from_geojson, histogram

GEOJSON_NAME = 'gravidez_adolescencia.geojson'

df = df_from_geojson(data=GEOJSON_NAME,
                     neighbourhoods='BAIRRO',
                     values='PROP_NASC_VIVOS_MAES_ADOLESCENTES',
                     metadata=['QTDE_NASCIDOS_VIVOS','QTDE_NASCIDOS_MAES_ADOLESCENTES'])

df_hist = histogram(df,'PROP_NASC_VIVOS_MAES_ADOLESCENTES')

df_hist.to_csv('../processed data/hist_gravidez.csv', index=False)

