import json
import pycurl
import logging
import numpy as np
import pandas as pd
from io import BytesIO
import geopandas as gpd
from fuzzywuzzy import process

# configurando logging no formato: (DateTime:Level:Arquivo:Mensagem)
log_format = '%(asctime)s:%(levelname)s:%(filename)s:%(message)s'

logging.basicConfig(filename=f'process_geojson.log',
                    # w -> sobrescreve o arquivo a cada log
                    # a -> não sobrescreve o arquivo
                    filemode='w',
                    level=logging.DEBUG,
                    format=log_format)

logger = logging.getLogger('root')



def bairros_canonicos() -> list:
    """ Retorna uma lista com o nome dos bairros de Fortaleza presentes na cartografia base do Datawrapper.

    Parameters
    ----------
    None

    Returns
    -------
    bairros_canonicos : list
        Lista com os nomes do bairros padronizados

    """

    # url que retorna a cartografia base dos bairros de Fortaleza do Datawrapper
    URL_BASEMAP_FORTALEZA = 'https://api.datawrapper.de/plugin/basemaps/brazil-fortaleza-barrios/name'
    
    # instanciando objeto pycurl e o buffer
    c = pycurl.Curl()
    buffer = BytesIO()

    # requisicao do tipo GET na API do Datawrapper que retorna o nomes dos bairros da cartografia base
    c.setopt(c.URL, URL_BASEMAP_FORTALEZA)
    c.setopt(c.WRITEDATA, buffer)
    c.perform()
    c.close()
    body = buffer.getvalue()
    bairros_canonicos = json.loads(body.decode('utf-8'))['data']['values']

    return bairros_canonicos

def correct_neighbourhood(df: pd.DataFrame, column_neighbourhood: str):
    ''' Padroniza o nome dos bairros de Fortaleza para os valores do Datawrapper usando fuzzy string matching.
    
    Parameters
    -----------
    df : pd.DataFrame
        Dataframe que devem ser padronizados os nomes dos bairros
    column_neighbourhood  : str 
        Coluna de df com o nome dos bairros

    Returns
    -------
    df_corrigido : pd.DataFrame
        Dataframe com o nome dos bairros corrigidos
    '''
    
    # casos especiais
    df.loc[df[column_neighbourhood] == 'CONJUNTO CEARA 2', column_neighbourhood] = 'CONJUNTO CEARA II'
    df.loc[df[column_neighbourhood] == 'VILA MANUEL SATIRO', column_neighbourhood] = 'MANOEL SATIRO'
    df.loc[df[column_neighbourhood] == 'RACHEL DE QUEIROZ', column_neighbourhood] = 'DENDE'
    
    # substituindo nomes dos bairros
    for bairro in df[column_neighbourhood]:
        result = process.extractOne(bairro, bairros_canonicos())
        df.loc[df[column_neighbourhood] == bairro, column_neighbourhood] = result[0]
        df_corrigido = df.copy()
        
        # realizando logging de cada uma das substituicoes, e caso haja incerteza, imprime um warning no arquivo de log
        if result[1] <=95:
            logger.warning(f'Alto nível de incerteza: {bairro} por {result[0]}. {result[1]}% de certeza. Realizar validacao.')
        else:
            logger.info(f'Substitui-se {bairro} por {result[0]} com {result[1]}% de certeza.')
    return df_corrigido

def df_from_geojson(data: str, neighbourhoods: str, values: str, metadata: list = []):
    '''
    Funcao que retorna um Pandas DataFrame com bairro (chave) e indicador (valor) para construcao de um mapa coropletico.

    Parameters
    ----------
    data : str
        Nome do arquivo de dados em formato geojson
    neighbourhoods : str
        Nome da coluna que contem os bairros
    values : str
        Nome da coluna que contem os valores
    metadata : list (optional)
        Lista com as colunas de dados que devem conter no hover dos bairros do Datawrapper
    
    Returns
    -------
    df : pd.DataFrame
        DataFrame com os dados processados
    '''
    gdf = gpd.read_file('../data/'+data)

    df = gdf[[neighbourhoods, values]]

    df_corrigido = correct_neighbourhood(df, 'BAIRRO')

    if len(metadata) > 0:
        for column in metadata:
            df_corrigido[column] = gdf[column]
    
    df_corrigido.to_csv(f'../processed data/table_{data.split(".")[0]}.csv', index=False)

    return df_corrigido

def histogram(df:pd.DataFrame, indicator:str) -> pd.DataFrame:
    bins=11
    min = df[indicator].min()
    max = df[indicator].max()
    histogram = np.histogram(df[indicator], range=(min,max), bins=bins)
    df_histogram = pd.DataFrame()
    df_histogram['limiar'] = histogram[1][1:]
    df_histogram['count_bairros'] = histogram[0]
    df_histogram['limiar'] = round(df_histogram['limiar'],1)
    return df_histogram

if __name__ == '__main__':
    # example
    df = df_from_geojson(data='gravidez_adolescencia.geojson', 
                         neighbourhoods='BAIRRO',
                         values='PROP_NASC_VIVOS_MAES_ADOLESCENTES',
                         metadata=['QTDE_NASCIDOS_VIVOS','QTDE_NASCIDOS_MAES_ADOLESCENTES'])
    print(df)
