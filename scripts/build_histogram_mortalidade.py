import pandas as pd
from process_geojson import df_from_geojson, histogram

GEOJSON_NAME = 'mortalidade_materna.geojson'

df = df_from_geojson(data=GEOJSON_NAME,
                     neighbourhoods='BAIRRO',
                     values='TAXA_OBITO_MATERNO',
                     metadata=['QTDE_OBITOS_MATERNOS','QTDE_NASCIDOS_VIVOS'])

df_hist = histogram(df,'TAXA_OBITO_MATERNO')

df_hist.to_csv('../processed data/hist_idade.csv', index=False)

df_indicators = pd.read_csv(f'../processed data/table_{GEOJSON_NAME.split(".")[0]}.csv').describe()
 
print(df_indicators)
