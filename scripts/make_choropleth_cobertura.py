import configparser
from datawrapper import Datawrapper
from process_geojson import df_from_geojson

# acessando o token da Datawrapper API
parser = configparser.ConfigParser()
parser.read('../pipeline.conf')
ACCESS_TOKEN = parser.get('datawrapper_token','token')

# metadados do mapa coropletico - description
FOLDER_ID = '224558'
SOURCE_NAME = 'Prefeitura de Fortaleza'
SOURCE_URL = 'https://www.fortaleza.ce.gov.br/'
BYLINE = 'Iplanfor'
TITLE = 'Área Verde por Bairro'

# nome do geojson
GEOJSON_NAME = 'cobertura_arborea_2021.geojson'

# instanciando objeto dw
dw = Datawrapper(access_token = ACCESS_TOKEN)

# construindo o DataFrame que ira compor o mapa coropletico
df = df_from_geojson(data=GEOJSON_NAME, 
                     neighbourhoods='BAIRRO',
                     values='% Área Total da Categoria',
                     metadata=['Área Total da Categoria na Cidade','Área km²'])

# customizando o mapa coropletico 
chart_info = dw.create_chart(title=TITLE, chart_type="d3-maps-choropleth", data=df, folder_id=FOLDER_ID)

dw.update_description(chart_info["id"],
                      source_name = SOURCE_NAME, 
                      source_url = SOURCE_URL,
                      byline = BYLINE)

properties = {
    "visualize": {
        "basemap": "brazil-fortaleza-barrios",
        "map-key-attr": "name"
        }
    }

dw.update_metadata(chart_info["id"], properties)

print(f'Mapa criado! Acesse https://datawrapper.de/chart/{chart_info["id"]}/visualize')
