import pandas as pd
from process_geojson import df_from_geojson, histogram

GEOJSON_NAME = 'idade_media_morte.geojson'

df = df_from_geojson(data=GEOJSON_NAME,
                     neighbourhoods='BAIRRO',
                     values='IDADE_MEDIA_AO_MORRER',
                     metadata=[])

df_hist = histogram(df,'IDADE_MEDIA_AO_MORRER')

df_hist.to_csv('../processed data/hist_idade.csv', index=False)

df_indicators = pd.read_csv(f'../processed data/table_{GEOJSON_NAME.split(".")[0]}.csv').describe()
 
print(df_indicators)
