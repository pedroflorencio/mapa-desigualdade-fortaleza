## Mapa da Desigualdade de Fortaleza

Proposta de *dashboard* para o Mapa da Desigualdade de Fortaleza, com o objetivo de identificar e quantificar as desigualdades nos bairros do município.

**Autor:** Pedro Florencio | Cientista de Dados @Iplanfor

**Acesso:** https://pedroflorencio.gitbook.io/mapa-da-desigualdade-de-fortaleza/ (Versão 0.0.1)

## Requisitos

Certifique-se de ter o Python instalado em sua máquina. Recomenda-se o uso de um ambiente virtual para evitar conflitos de dependências.

## Instalação

1. Clone o repositório para sua máquina local:
```bash
https://gitlab.com/pedroflorencio/mapa-desigualdade-fortaleza.git
```

2. Navegue até o diretório do projeto:
```bash
cd mapa-desigualdade-fortaleza
```
3. Instale as dependências:
```bash
pip install -r requirements.txt
```

## Estrutura do Projeto
- *make_choropleth_<nome_do_indicador>*: Contém o código que produz os mapas coropléticos de cada indicador
- *build_histogram_<nome_do_indicador>*: Contém o código que produz os histogramas e tabelas de cada indicador
- scripts/: Armazena os scripts.
- processed data/: Armazena os dados processados das tabelas e histogramas.
- *requirements.txt*: Lista as dependências do projeto.

## Tecnologias
Algumas das principais tecnologias e frameworks utilizados no projeto.<br/><br/>
<div>
    <img src="https://img.shields.io/badge/Python-3776AB?style=for-the-badge&logo=python&logoColor=white"
    height="22px" />
    <img src="https://img.shields.io/badge/pandas-%23150458.svg?style=for-the-badge&logo=pandas&logoColor=white" height="22px" />
    <img src="https://img.shields.io/badge/Plotly-%233F4F75.svg?style=for-the-badge&logo=plotly&logoColor=white" height="22px" />
    <img src="https://img.shields.io/badge/AWS-%23FF9900.svg?style=for-the-badge&logo=amazon-aws&logoColor=white" height="22px" />
</div>

## Contribuições
Contribuições são bem-vindas! Se você encontrar problemas ou tiver melhorias para sugerir, por favor, abra uma issue ou envie um pull request.

## Licença
Este projeto é distribuído sob a licença MIT. Consulte o arquivo LICENSE para obter mais informações.
